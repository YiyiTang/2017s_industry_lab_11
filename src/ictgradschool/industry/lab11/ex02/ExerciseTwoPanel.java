package ictgradschool.industry.lab11.ex02;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple JPanel that allows users to add or subtract numbers.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseTwoPanel extends JPanel {

    private JButton addBtn;
    private JButton subtractBtn;
    private JTextField operandOne;
    private JTextField operandTwo;
    private JTextField result;

    /**
     * Creates a new ExerciseFivePanel.
     */
    public ExerciseTwoPanel() {
        setBackground(Color.white);

        operandOne = new JTextField(10);
        add(operandOne);
        operandTwo = new JTextField(10);
        add(operandTwo);
        addBtn = new JButton("Add");
        add(addBtn);
        subtractBtn = new JButton("Subtract");
        add(subtractBtn);
        JLabel label = new JLabel("Result:");
        add(label);
        result = new JTextField(10);
        add(result);

        addBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double operand1 = Double.valueOf(operandOne.getText());
                double operand2 = Double.valueOf(operandTwo.getText());
                result.setText("" + roundTo2DecimalPlaces(operand1+ operand2));
            }
        });

        subtractBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                double operand1 = Double.valueOf(operandOne.getText());
                double operand2 = Double.valueOf(operandTwo.getText());
                result.setText("" + roundTo2DecimalPlaces(operand1 - operand2));
            }
        });

    }

    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}