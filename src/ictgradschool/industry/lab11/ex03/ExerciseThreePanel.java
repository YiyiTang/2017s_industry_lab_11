package ictgradschool.industry.lab11.ex03;

import javax.swing.*;
import java.awt.*;

/**
 * A JPanel that draws some houses using a Graphics object.
 *
 * TODO Complete this class. No hints this time :)
 */
public class ExerciseThreePanel extends JPanel {

    /** All outlines should be drawn this color. */
    private static final Color OUTLINE_COLOR = Color.black;

    /** The main "square" of the house should be drawn this color. */
    private static final Color MAIN_COLOR = new Color(255, 229, 204);

    /** The door should be drawn this color. */
    private static final Color DOOR_COLOR = new Color(150, 70, 20);

    /** The windows should be drawn this color. */
    private static final Color WINDOW_COLOR = new Color(255, 255, 153);

    /** The roof should be drawn this color. */
    private static final Color ROOF_COLOR = new Color(255, 153, 51);

    /** The chimney should be drawn this color. */
    private static final Color CHIMNEY_COLOR = new Color(153, 0, 0);

    private static final int rectW = 10;
    private static final int rectH = 7;
    private static final int winRectSize = 1;
    private static final int[] triangleTop = {5,0};
    private static final int[] triangleLeft = {0,5};
    private static final int[] triangleRight = {10,5};


    /**
     * Creates a new ExerciseFourPanel.
     */
    public ExerciseThreePanel() {
        setBackground(Color.white);
    }

    /**
     * Draws eight houses, using the method that you implement for this exercise.
     * @param g
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        drawHouse(g, 125, 177, 3);
        drawHouse(g, 199, 193, 7);
        drawHouse(g, 292, 55, 5);
        drawHouse(g, 29, 110, 8);
        drawHouse(g, 379, 386, 7);
        drawHouse(g, 127, 350, 12);
        drawHouse(g, 289, 28, 2);
        drawHouse(g, 300, 150, 16);
    }

    /**
     * Draws a single house, with its top-left at the given coordinates, and with the given size multiplier.
     *
     * @param g the {@link Graphics} object to use for drawing
     * @param left the x coordinate of the house's left side
     * @param top the y coordinate of the top of the house's roof
     * @param size the size multipler. If 1, the house should be drawn as shown in the grid in the lab handout.
     */
    private void drawHouse(Graphics g, int left, int top, int size) {

        // TODO Draw a house, as shown in the lab handout.

        drawBaseRect(g, left, top, size);
        drawLeftWindow(g, left, top, size);
        drawRightWindow(g, left, top, size);
        drawDoor(g, left, top, size);
        drawChimney(g, left, top, size);

        drawRoof(g, left, top, size);


    }

    private void drawBaseRect(Graphics g, int left, int top, int size) {
        int x = left;
        int y = top + 5 * size;

        g.setColor(MAIN_COLOR);
        g.fillRect(x, y, rectW * size, rectH * size);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(x, y, rectW * size, rectH * size);
    }

    private void drawDoor(Graphics g, int left, int top, int size) {
        int x = left + 4 * size;
        int y = top + 8 * size;

        g.setColor(DOOR_COLOR);
        g.fillRect(x, y, 2 * size, 4 * size);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(x, y, 2 * size, 4 * size);
    }

    private void drawChimney(Graphics g, int left, int top, int size) {
        int x = left + 7 * size;
        int y = top + 1 * size;

        g.setColor(CHIMNEY_COLOR);
        g.fillRect(x, y, 1 * size, 2 * size);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(x, y, 1 * size, 2 * size);
    }

    private void drawRoof(Graphics g, int left, int top, int size) {
        int[] xpos = new int[3];
        xpos[0] = left + 5 * size;
        xpos[1] = left;
        xpos[2] = left + 10 * size;

        int[] ypos = new int[3];
        ypos[0] = top;
        ypos[1] = top + 5 * size;
        ypos[2] = top + 5 * size;

        g.setColor(ROOF_COLOR);
        g.fillPolygon(xpos, ypos, 3);
        g.setColor(OUTLINE_COLOR);
        g.drawPolygon(xpos, ypos, 3);
    }

    private void drawLeftWindow(Graphics g, int left, int top, int size) {

        drawWindow(g, left + size, top + 7 * size, size);
    }

    private void drawRightWindow(Graphics g, int left, int top, int size) {
        drawWindow(g, left + 7*size, top + 7 * size, size);
    }

    private void drawWindow(Graphics g, int left, int top, int size) {
//        int x = left + size;
//        int y = top + 7 * size;
        int x = left;
        int y = top;

        g.setColor(WINDOW_COLOR);
        g.fillRect(x, y, size, size);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(x, y, size, size);

        int x2 = x + size;
        g.setColor(WINDOW_COLOR);
        g.fillRect(x2, y, size, size);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(x2, y, size, size);

        int y2 = y + size;
        g.setColor(WINDOW_COLOR);
        g.fillRect(x, y2, size, size);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(x, y2, size, size);

        g.setColor(WINDOW_COLOR);
        g.fillRect(x2, y2, size, size);
        g.setColor(OUTLINE_COLOR);
        g.drawRect(x2, y2, size, size);

    }
}