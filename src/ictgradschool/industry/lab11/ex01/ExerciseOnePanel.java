package ictgradschool.industry.lab11.ex01;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * A simple GUI app that does BMI calculations.
 */
public class ExerciseOnePanel extends JPanel implements ActionListener {

    // TODO Declare JTextFields and JButtons as instance variables here.
    private JButton calculateBMIButton;
    private JButton calculateHealthyWeight;
    private JTextField bmiTF;
    private JTextField heightTF;
    private JTextField weightTF;
    private JTextField healthyTF;

    /**
     * Creates a new ExerciseOnePanel.
     */
    public ExerciseOnePanel() {
        setBackground(Color.white);

        //setBounds(0,0, 1200, 100);

        // TODO Construct JTextFields and JButtons.
        // HINT: Declare them as instance variables so that other methods in this class (e.g. actionPerformed) can
        // also access them.
        calculateBMIButton = new JButton("Calculate BMI");
        calculateHealthyWeight = new JButton("Calculate Healthy Weight");
        bmiTF = new JTextField(10);
        bmiTF.setMinimumSize(new Dimension(100, 30));
        heightTF = new JTextField(10);
        weightTF = new JTextField(10);
        weightTF.setMinimumSize(new Dimension(100, 30));
        healthyTF = new JTextField(10);


        // TODO Declare and construct JLabels
        // Note: These ones don't need to be accessed anywhere else so it makes sense just to declare them here as
        // local variables, rather than instance variables.
        JLabel heightLabel = new JLabel("Height in metres:");
        JLabel weightLabel = new JLabel("Weight in kilograms:");
        JLabel bmiLabel = new JLabel("Your body BMI is:");
        JLabel healthyLabel = new JLabel("Maximum Healthy Weight for your Height:");

        // TODO Add JLabels, JTextFields and JButtons to window.
        // Note: The default layout manager, FlowLayout, will be fine (but feel free to experiment with others if you want!!)




        GridBagLayout grid = new GridBagLayout();
        setLayout(grid);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        this.add(heightLabel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        this.add(heightTF, gbc);
        gbc.gridx = 2;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        this.add(weightLabel, gbc);
        gbc.gridx = 3;
        gbc.gridy = 0;
        gbc.gridwidth = 1;
        this.add(weightTF, gbc);

        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 2;
        this.add(bmiLabel, gbc);
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        this.add(bmiTF, gbc);
        gbc.gridx = 2;
        gbc.gridy = 1;
        gbc.gridwidth = 1;
        this.add(calculateBMIButton, gbc);

        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 2;
        this.add(healthyLabel, gbc);

        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        this.add(healthyTF, gbc);
        gbc.gridx = 2;
        gbc.gridy = 2;
        gbc.gridwidth = 1;
        this.add(calculateHealthyWeight, gbc);



//        this.add(weightLabel);
//        this.add(weightTF);
        // TODO Add Action Listeners for the JButtons
        calculateBMIButton.addActionListener(this);
        calculateHealthyWeight.addActionListener(this);

    }


    /**
     * When a button is clicked, this method should detect which button was clicked, and display either the BMI or the
     * maximum healthy weight, depending on which JButton was pressed.
     */
    public void actionPerformed(ActionEvent event) {

        // TODO Implement this method.
        // Hint #1: event.getSource() will return the button which was pressed.
        // Hint #2: JTextField's getText() method will get the value in the text box, as a String.
        // Hint #3: JTextField's setText() method will allow you to pass it a String, which will be diaplayed in the text box.
        if (event.getSource() == calculateBMIButton) {
            double height = Double.valueOf(heightTF.getText());
            double weight = Double.valueOf(weightTF.getText());
            double bmi = weight / (height*height);
            bmiTF.setText("" + roundTo2DecimalPlaces(bmi));
        } else if (event.getSource() == calculateHealthyWeight) {
            double height = Double.valueOf(heightTF.getText());

            healthyTF.setText("" + roundTo2DecimalPlaces(24.9 * height * height));
        }

    }


    /**
     * A library method that rounds a double to 2dp
     *
     * @param amount to round as a double
     * @return the amount rounded to 2dp
     */
    private double roundTo2DecimalPlaces(double amount) {
        return ((double) Math.round(amount * 100)) / 100;
    }

}